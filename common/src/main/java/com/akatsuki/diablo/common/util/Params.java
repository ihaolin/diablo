package com.akatsuki.diablo.common.util;

import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import java.util.Map;

/**
 * Author: haolin
 * On: 2/9/15
 */
public class Params {

    /**
     * 过滤map中值为NULL或者空字符串的Entry
     * @param map map集
     * @return 过滤后的map
     */
    public static Map<String, Object> filterNullOrEmpty(Map<String, Object> map){
        Map<String, Object> filtered = Maps.newHashMap();
        Object val;
        for (Map.Entry<String, Object> entry : map.entrySet()){
            val = entry.getValue();
            if (val == null
                    || (val instanceof String && Strings.isNullOrEmpty((String)val))){
                continue;
            }
            filtered.put(entry.getKey(), entry.getValue());
        }
        return filtered;
    }
}
