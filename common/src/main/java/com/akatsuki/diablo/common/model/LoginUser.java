package com.akatsuki.diablo.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * 当前登录用户
 * Created by haolin on 1/6/15.
 */
@Data @NoArgsConstructor @AllArgsConstructor
public class LoginUser {

    private Long id;

    private Integer type;

    private Integer status;

    private String nickname;

    private String avatar;

    private List<String> roles;

}
