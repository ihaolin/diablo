package com.akatsuki.diablo.common.model;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * common page class
 * Author: haolin
 * On: 11/7/14
 */
public class Page<T> {

    private  Integer offset;

    private Integer limit;

    public Page() {
        this(1, 20);
    }

    public Page(Integer pageNo, Integer pageSize) {
       this(pageNo, pageSize, Integer.MAX_VALUE);
    }

    public Page(Integer pageNo, Integer pageSize, Integer maxPage) {
        pageNo = pageNo == null || pageNo < 0 ? 1 : pageNo;
        pageNo = pageNo > maxPage ? maxPage : pageNo;
        pageSize = pageSize == null || pageSize < 0 ? 20 : pageSize;
        limit = pageSize > 0 ? pageSize : 20;
        offset = (pageNo - 1) * pageSize;
    }

    public static final Page of(Integer pageNo, Integer pageSize) {
        return new Page(pageNo, pageSize);
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Map<String, Integer> toMap() {
        return ImmutableMap.of("offset", offset, "limit", limit);
    }
}
