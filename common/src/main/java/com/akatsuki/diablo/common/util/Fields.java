package com.akatsuki.diablo.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * Fields Util
 * Author: haolin
 * On: 11/11/14
 */
public abstract class Fields {

    private static final Logger log = LoggerFactory.getLogger(Fields.class);

    private static final Unsafe unsafe = getUnsafe();

    private static Unsafe getUnsafe() {
        try {
            Field f = Unsafe.class.getDeclaredField("theUnsafe");
            f.setAccessible(true);
            Unsafe unsafe = (Unsafe) f.get(null);
            return unsafe;
        } catch (Exception e) {
            log.error("failed to get Unsafe instance.");
            return null;
        }
    }

    /**
     * put field to target object
     * @param target target object
     * @param name field name
     * @param value field valiue
     */
    public static void put(Object target, String name, Object value) throws NoSuchFieldException {
        try {
            Field field = target.getClass().getDeclaredField(name);
            long fieldOffset = unsafe.objectFieldOffset(field);
            unsafe.putObject(target, fieldOffset, value);
        } catch (NoSuchFieldException e) {
            log.error("object({}) don't have field(name={})",
                    target, name);
            throw e;
        }
    }

    /**
     * get field of target object
     * @param target target object
     * @param name field name
     * @return the field value
     */
    public static <T> T get(Object target, String name) throws NoSuchFieldException {
        try {
            Field field = target.getClass().getDeclaredField(name);
            long fieldOffset = unsafe.objectFieldOffset(field);
            return (T)unsafe.getObject(target, fieldOffset);
        } catch (NoSuchFieldException e) {
            log.error("object({}) don't have field(name={})",
                    target, name);
            throw e;
        } catch (Exception e){
            log.error("failed to get field(name={}) of object({})",
                    name, target);
            e.printStackTrace();
            return null;
        }
    }
}
