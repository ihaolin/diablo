package com.akatsuki.diablo.common.json;

import com.fasterxml.jackson.databind.JavaType;
import com.google.common.collect.Lists;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * a jsoner for object & hashmap
 * @param <T>
 */
public class StringHashJsoner<T> {

    private final Jsoner jsoner;

    private final JavaType userType;

    private final JavaType mapType;

    public StringHashJsoner(Class<T> type) {
        this.jsoner = Jsoner.nonDefaultJsoner();
        this.mapType = jsoner.createCollectionType(HashMap.class, String.class, String.class);
        this.userType = jsoner.getMapper().getTypeFactory().constructType(type);
    }

    /**
     * convert hash to target object
     * @param hash hash map object
     * @return from hash
     */
    public T fromHash(Map<String, String> hash) {
        if (hash.isEmpty()) return null;
        return jsoner.getMapper().convertValue(hash, userType);
    }

    /**
     * convert object to hash map
     * @param object target object
     * @return hash map object
     */
    public Map<String, String> toHash(T object) {
        Map<String, String> hash = jsoner.getMapper().convertValue(object, mapType);
        //remove null values
        List<String> nullKeys = Lists.newArrayListWithCapacity(hash.size());
        for (String key : hash.keySet()) {
            if (hash.get(key) == null) {
                nullKeys.add(key);
            }
        }
        for (String nullKey : nullKeys) {
            hash.remove(nullKey);
        }
        return hash;
    }
}