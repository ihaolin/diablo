package com.akatsuki.diablo.common.util;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Some common splitters
 */
public class Splitters {

    public static final Splitter DOT = Splitter.on(".").omitEmptyStrings().trimResults();

    public static final Splitter COMMA = Splitter.on(",").omitEmptyStrings().trimResults();

    public static final Splitter COLON = Splitter.on(":").omitEmptyStrings().trimResults();

    public static final Splitter AT = Splitter.on("@").omitEmptyStrings().trimResults();

    public static final Splitter SLASH = Splitter.on("/").omitEmptyStrings().trimResults();

    public static final Splitter SPACE = Splitter.on(" ").omitEmptyStrings().trimResults();

    /**
     * split string to list long
     * @param src char sequence source
     * @param splitter splitter
     * @return list long
     */
    public static List<Long> split2Long(CharSequence src, Splitter splitter) {
        List<String> ss = splitter.splitToList(src);
        List<Long> res = Lists.newArrayListWithCapacity(ss.size());
        for (String s : ss) {
            res.add(Long.parseLong(s));
        }
        return res;
    }

    /**
     * split string to list integer
     * @param src char sequence source
     * @param splitter splitter
     * @return list integer
     */
    public static List<Integer> split2Int(CharSequence src, Splitter splitter) {
        List<String> ss = splitter.splitToList(src);
        List<Integer> res = Lists.newArrayListWithCapacity(ss.size());
        for (String s : ss) {
            res.add(Integer.parseInt(s));
        }
        return res;
    }
}
