package com.akatsuki.diablo.common.util;

import static com.akatsuki.diablo.common.util.Args.*;

/**
 * A number operate util
 * Author: haolin
 * On: 12/11/14
 */
public class Numbers {

    /**
     * a / b
     * @param a dividend
     * @param b divisor
     * @param scale decimal digits
     * @return result string
     */
    public static String divide(Integer a, Integer b, int scale){
        return divide(a, b, scale, 1);
    }

    /**
     * a / b
     * @param a dividend
     * @param b divisor
     * @param scale decimal digits
     * @param factor multiplication factor
     * @return result string
     */
    public static String divide(Integer a, Integer b, int scale, int factor){
        if (isNull(a) || isNull(b)){
            return "";
        }
        Object result = ((double)a / (double)b) * factor;
        return String.format("%."+ scale + "f", result);
    }
}
