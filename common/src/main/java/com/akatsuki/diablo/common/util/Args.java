package com.akatsuki.diablo.common.util;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

import java.util.Collection;

/**
 * Arguments check util
 * Author: haolin
 * On: 12/11/14
 */
public class Args {

    /**
     * check the collection is null or empty
     * @param t collection object
     * @param <T>
     * @return true if collection object is null or empty, or false
     */
    public static <T extends Collection> boolean isNullOrEmpty(T t) {
        return isNull(t) || isEmpty(t);
    }

    /**
     * check the collection is empty or not
     * @param t collection object
     * @param <T>
     * @return true if collection object is empty
     */
    public static  <T extends Collection> boolean isEmpty(T t) {
        return t.isEmpty();
    }

    /**
     * check object is null or not
     * @param o object
     * @return true if object is null, or false
     */
    public static boolean isNull(Object o) {
        return null == o;
    }

    /**
     * check object is null or not
     * @param o object
     * @return true if object isn't null, or false
     */
    public static boolean notNull(Object o) {
        return null != o;
    }

    /**
     * check string is null or empty
     * @param s string
     * @return true if string is null or empty
     */
    public static boolean isNullOrEmpty(String s) {
        return Strings.isNullOrEmpty(s);
    }

    /**
     * check string isn't null and empty
     * @param s string
     * @return true if string isn't null and empty, or false
     */
    public static boolean notNullAndEmpty(String s) {
        return !isNullOrEmpty(s);
    }

    public static <T extends Collection> boolean notEmpty(T l) {
        return notNull(l) && !l.isEmpty();
    }

    /**
     * check number > 0 or not
     * @param n number
     * @return true if n != null and n > 0, or false
     */
    public static boolean gt0(Number n) {
        return null != n && n.doubleValue() > 0;
    }

    /**
     * check number >= 0 or not
     * @param n number
     * @return true if n != null and n >= 0, or false
     */
    public static boolean ge0(Number n) {
        return null != n && n.doubleValue() >= 0;
    }

    /**
     * check number < 0or not
     * @param n number
     * @return true if n != null and n < 0, or false
     */
    public static boolean lt0(Number n) {
        return null != n && n.doubleValue() < 0;
    }

    /**
     * check number <= 0 or not
     * @param n number
     * @return true if n != null and n <= 0, or false
     */
    public static boolean le0(Number n) {
        return null != n && n.doubleValue() <= 0;
    }

    /**
     * check left is equals with right or not
     * @param left left object
     * @param right right object
     * @param <T>
     * @return true if left.equal(right);
     */
    public static <T> boolean equal(T left, T right) {
        return Objects.equal(left, right);
    }
}
