package com.akatsuki.diablo.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 数据库分页类
 * @param <T>
 */
@Data
@NoArgsConstructor @AllArgsConstructor
public class Paging<T> implements Serializable {

    private static final long serialVersionUID = 7544274721272147458L;

    private Long total;

    private List<T> data;

    /**
     * 构建空分页对象
     * @param <T>
     * @return
     */
    public static <T> Paging<T> empty() {
        return new Paging<T>(0L, Collections.<T>emptyList());
    }
}