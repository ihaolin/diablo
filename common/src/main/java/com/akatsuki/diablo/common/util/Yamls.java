package com.akatsuki.diablo.common.util;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.yaml.snakeyaml.Yaml;
import java.io.File;
import java.io.IOException;

/**
 * Author: haolin
 * At: 1/15/15
 */
public class Yamls {

    private static final Yaml y = new Yaml();

    public static <T> T load(String source, Class<T> c){
        return y.loadAs(source, c);
    }

    public static <T> T load(File file, Class<T> c) throws IOException {
        return load(Files.asCharSource(file, Charsets.UTF_8).read(),c);
    }
}