package com.akatsuki.diablo.common.exception;

/**
 * Author: haolin
 * On: 1/11/15
 */
public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = -8251986376649595188L;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
