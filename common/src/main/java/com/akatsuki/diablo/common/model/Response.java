package com.akatsuki.diablo.common.model;

import com.google.common.base.MoreObjects;

import java.io.Serializable;

/**
 * Common Response Object
 * @param <T>
 */
public class Response<T> implements Serializable {

    private static final long serialVersionUID = -5067193489858556282L;

    /**
     * true indicate request is ok, or false
     */
    private boolean success;

    /**
     * response data
     */
    private T result;

    /**
     * indicate request error if success is false
     */
    private String error;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.success = true;
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.success = false;
        this.error = error;
    }

    /**
     * response is ok
     * @param result
     * @return ok response
     */
    public Response<T> ok(T result){
        this.success = true;
        this.result = result;
        return this;
    }

    /**
     * response isn't ok
     * @param error error code
     * @return not ok response
     */
    public Response<T> isntOk(String error){
        this.success = false;
        this.error = error;
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("success", success)
                .add("result", result)
                .add("error", error)
                .omitNullValues()
                .toString();
    }
}
