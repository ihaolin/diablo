package com.akatsuki.diablo.common.util;

public enum Protocols {

    HTTP("http://"),
    HTTPS("https://"),
    CLASSPATH("classpath:"),
    SERVLET("resource:"),
    FILE("");

    private String prefix;

    private Protocols(String prefix) {
        this.prefix = prefix;
    }

    public static Protocols analyze(String uri) {
        String lowerUri = uri.toLowerCase();
        if (lowerUri.startsWith(HTTP.prefix)) {
            return HTTP;
        }
        if (lowerUri.startsWith(HTTPS.prefix)) {
            return HTTPS;
        }
        if (lowerUri.startsWith(SERVLET.prefix)) {
            return SERVLET;
        }
        if (lowerUri.startsWith(CLASSPATH.prefix)) {
            return CLASSPATH;
        }
        return FILE;
    }

    public static String removeProtocol(String uri) {
        return removeProtocol(uri, analyze(uri));
    }

    public static String removeProtocol(String uri, Protocols protocol) {
        return uri.substring(protocol.prefix.length());
    }
}