package com.akatsuki.diablo.web.handlebars;

/**
 * Author: haolin
 * At: 1/6/15
 */
public final class RenderConstants {

    public static final String LOCALE = "_LOCALE_";

    // 配置数据的key，用于嵌套调用时清理上下文
    public static final String CDATA_KEYS = "_CDATA_KEYS_";

    //服务调用数据
    public static final String DATA = "_DATA_";

    public static final String ERROR = "_ERROR_";

    public static final String USER = "_USER_";

    private RenderConstants(){}
}
