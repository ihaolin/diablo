package com.akatsuki.diablo.web.interceptor;

import com.akatsuki.diablo.web.context.AppContext;
import com.google.common.net.HttpHeaders;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Author: haolin
 * At: 1/13/15
 */
public class AppInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        AppContext.setHost(request.getHeader(HttpHeaders.HOST));
        AppContext.setLocale(request.getLocale().toString());
        return Boolean.TRUE;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        AppContext.clear();
    }
}
