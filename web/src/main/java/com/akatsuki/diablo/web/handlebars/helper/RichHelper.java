package com.akatsuki.diablo.web.handlebars.helper;

import com.akatsuki.diablo.web.handlebars.HandlebarsViewResolver;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import org.joda.time.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 通用的Handlerbars标签
 * Author: haolin
 * At: 1/6/15
 */
@Component
public class RichHelper {

    /**
     * 判断第1，2变量是否相等
     */
    private static final String EQ = "eq";
    /**
     * 判断第1，2变量是否不相等
     */
    private static final String NOT_EQ = "neq";
    /**
     * 日期格式化
     */
    private static final String FORMAT_DATE ="fmtDate";
    /**
     * 时间差格式化
     */
    private static final String FORMAT_PERIOD = "fmtPeriod";
    /**
     * 普通文本(跳过解析)
     */
    private static final String PLAIN_TEXT = "plaintext";
    /**
     * 求子串
     */
    private static final String SUB_STR = "substr";

    @Autowired
    private HandlebarsViewResolver viewResolver;

    @PostConstruct
    public void init() {

        viewResolver.registerHelper(EQ, new Helper<Object>() {
            @Override
            public CharSequence apply(Object source, Options options)
                    throws IOException {
                if (Objects.equal(String.valueOf(source),
                        String.valueOf(options.param(0))))
                    return options.fn();
                else
                    return options.inverse();
            }
        });

        viewResolver.registerHelper(NOT_EQ, new Helper<Object>() {
            @Override
            public CharSequence apply(Object source, Options options)
                    throws IOException {
                if (!Objects.equal(String.valueOf(source),
                        String.valueOf(options.param(0))))
                    return options.fn();
                else
                    return options.inverse();
            }
        });

        viewResolver.registerHelper(FORMAT_DATE, new Helper<Date>() {
            Map<String, SimpleDateFormat> sdfMap = ImmutableMap.of("gmt",
                    new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy"), "day",
                    new SimpleDateFormat("yyyy-MM-dd"), "default",
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));

            @Override
            public CharSequence apply(Date date, Options options)
                    throws IOException {
                if (date == null) {
                    return "";
                }
                String format = options.param(0, "default");
                if (format.equals("ut")) {
                    return Long.toString(date.getTime());
                }
                if (!sdfMap.containsKey(format)) {
                    sdfMap.put(format, new SimpleDateFormat(format));
                }
                return sdfMap.get(format).format(date);
            }
        });

        viewResolver.registerHelper(FORMAT_PERIOD, new Helper<Date>() {

            @Override
            public CharSequence apply(Date date, Options options)
                    throws IOException {
                DateTime target = new DateTime(date);
                DateTime now = DateTime.now();

                int period = 0;
                period = Years.yearsBetween(target, now).getYears();
                if (period > 0) return period + "年前";

                period = Days.daysBetween(target, now).getDays();
                if (period > 0) return period + "天前";

                period = Hours.hoursBetween(target, now).getHours();
                if (period > 0) return period + "小时前";

                period = Minutes.minutesBetween(target, now).getMinutes();
                if (period > 0) return period + "分钟前";

                return "刚刚";
            }
        });

        viewResolver.registerHelper(PLAIN_TEXT, new Helper<Void>() {
            @Override
            public CharSequence apply(Void context, Options options)
                    throws IOException {
                return options.fn.text();
            }
        });

        viewResolver.registerHelper(SUB_STR, new Helper<String>() {
            @Override
            public CharSequence apply(String str, Options options)
                    throws IOException {
                int paramLen = options.params.length;
                if (paramLen == 1){
                    Integer startIndex = 0;
                    try {
                        startIndex = Integer.valueOf(String.valueOf(options.params[0]));
                    } catch (NumberFormatException e) {
                    }
                    return str.substring(startIndex);
                } else if(paramLen == 2){
                    Integer startIndex = 0;
                    Integer endIndex = str.length();
                    try {
                        startIndex = Integer.valueOf(String.valueOf(options.params[0]));
                    } catch (NumberFormatException e) {}
                    try {
                        endIndex = Integer.valueOf(String.valueOf(options.params[1]));
                    } catch (NumberFormatException e) {}
                    endIndex = endIndex > str.length() ? str.length() : endIndex;
                    return str.substring(startIndex, endIndex);
                }
                return str;
            }
        });
    }
}
