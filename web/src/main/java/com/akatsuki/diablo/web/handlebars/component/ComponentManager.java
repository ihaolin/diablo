package com.akatsuki.diablo.web.handlebars.component;

import com.akatsuki.diablo.common.util.Yamls;
import com.akatsuki.diablo.web.handlebars.HandlebarsViewResolver;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.google.common.collect.Maps;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Author: haolin
 * At: 1/15/15
 */
@Component @Slf4j
public class ComponentManager {

    private Map<String, HandlebarsComponent> componentMappings;

    private Map<String, HandlebarsMapping> viewMappings;

    private String componentConfigPath;

    public void setComponentConfigPath(String componentConfigPath) {
        this.componentConfigPath = componentConfigPath;
    }

    @Autowired
    private HandlebarsViewResolver viewResolver;

    @PostConstruct
    public void init(){
        loadComponents();
    }

    private void loadComponents(){
        String configPath = Strings.isNullOrEmpty(componentConfigPath) ?
                viewResolver.getAssetsHome() + "/conf/components.yml" : componentConfigPath;
        try {
            ComponentConfig componentCfg = Yamls.load(new File(configPath), ComponentConfig.class);
            componentMappings = Maps.newHashMapWithExpectedSize(componentCfg.components.size());
            for (HandlebarsComponent component : componentCfg.components){
                log.debug("load handlebars component[{}].", component);
                componentMappings.put(component.getPath(), component);
            }
            viewMappings = Maps.newHashMapWithExpectedSize(componentCfg.mappings.size());
            for (HandlebarsMapping mapping : componentCfg.mappings){
                viewMappings.put(mapping.getPath(), mapping);
            }
        } catch (IOException e){
            log.warn("failed to load components from: {}, cause: {}",
                    configPath, Throwables.getStackTraceAsString(e));
        }
    }

    /**
     * 根据path查询HandlebarsComponent
     * @param path 路径
     * @return HandlebarsComponent
     */
    public HandlebarsComponent findComponent(String path){
        return componentMappings.get(path);
    }

    @Data
    public static class ComponentConfig {

        /**
         * 组件
         */
        private List<HandlebarsComponent> components;

        /**
         * URL映射
         */
        private List<HandlebarsMapping> mappings;

    }
}
