package com.akatsuki.diablo.web.exception;

/**
 * Author: haolin
 * At: 1/12/15
 */
public class UnAuthorizedException extends RuntimeException{

    private static final long serialVersionUID = 8745264094248052462L;

    public UnAuthorizedException() {
    }

    public UnAuthorizedException(String message) {
        super(message);
    }

    public UnAuthorizedException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnAuthorizedException(Throwable cause) {
        super(cause);
    }

}
