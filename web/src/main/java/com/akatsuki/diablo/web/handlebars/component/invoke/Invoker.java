package com.akatsuki.diablo.web.handlebars.component.invoke;

import com.akatsuki.diablo.common.model.Response;
import com.akatsuki.diablo.web.handlebars.RenderConstants;
import com.akatsuki.diablo.web.handlebars.component.ComponentManager;
import com.akatsuki.diablo.web.handlebars.component.HandlebarsComponent;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Set;

/**
 * Author: haolin
 * At: 1/15/15
 */
@Component @Slf4j
public class Invoker {

    @Autowired
    private ComponentManager componentManager;

    @Autowired
    private SpringInvoker springInvoker;

    private final Set<String> unUsedContextKeys = Sets.newHashSet(
        "springMacroRequestContext",
            RenderConstants.USER,
         "org.springframework.validation.BindingResult._USER_"
    );

    private Map<String, HandlebarsComponentInvoker> invokers = Maps.newHashMapWithExpectedSize(1);

    @PostConstruct
    public void init(){
        invokers.put(HandlebarsComponent.Type.SPRING.value(), springInvoker);
    }

    /**
     * 调用组件
     * @param componentPath 组件path
     * @param context 上下文
     * @return 结果
     */
    public Response<?> invoke(String componentPath, Map<String, Object> context){
        HandlebarsComponent component = componentManager.findComponent(componentPath);
        if (component == null){
            log.warn("component({}) isn't found, ", componentPath);
            return null;
        }

        return invokers.get(component.getType()).invoke(component, filterContext(context));
    }

    /**
     * 过滤一些无用的参数
     * @param context 上下文
     * @return 过滤后的参数
     */
    private Map<String, Object> filterContext(Map<String, Object> context) {
        Map<String, Object> temp = Maps.newHashMapWithExpectedSize(context.size() - unUsedContextKeys.size());
        for (String key : context.keySet()){
            if (!unUsedContextKeys.contains(key)){
                temp.put(key, context.get(key));
            }
        }
        return temp;
    }
}
