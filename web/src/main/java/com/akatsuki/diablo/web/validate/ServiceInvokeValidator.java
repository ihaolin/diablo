package com.akatsuki.diablo.web.validate;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;

/**
 * 服务调用验证器
 * Author: haolin
 * On: 9/12/14
 */
@Component
public class ServiceInvokeValidator {

    public void validate(final JoinPoint point){
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        InvokeValidator.instance().validateParams(point.getTarget(), method, point.getArgs());
    }
}
