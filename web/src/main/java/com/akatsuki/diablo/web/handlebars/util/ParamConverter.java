package com.akatsuki.diablo.web.handlebars.util;

import com.akatsuki.diablo.common.model.LoginUser;
import com.akatsuki.diablo.web.context.LoginContext;
import com.akatsuki.diablo.web.exception.ComponentExcpetion;
import com.akatsuki.diablo.web.exception.UnAuthorizedException;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.support.DefaultConversionService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: haolin
 * At: 1/26/15
 */
public class ParamConverter {

    private static ObjectMapper mapper;

    private static Map<String, Class<?>> primitiveClassMap = new HashMap<String, Class<?>>();

    private static DefaultConversionService conversionService = new DefaultConversionService();

    static {

        mapper = new ObjectMapper();

        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        //忽略对象中没有的json属性
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        primitiveClassMap.put("int", int.class);
        primitiveClassMap.put("long", long.class);
        primitiveClassMap.put("short", short.class);
        primitiveClassMap.put("byte", byte.class);
        primitiveClassMap.put("char", char.class);
        primitiveClassMap.put("boolean", boolean.class);
        primitiveClassMap.put("float", float.class);
        primitiveClassMap.put("double", double.class);
    }

    /**
     * 判断对象是否是基本类型
     * @param obj
     * @return 若是基本类型返回true, 反之false
     */
    public static boolean isPrimitive(Object obj) {
        if (obj instanceof Class) {
            return (((Class) obj).isPrimitive()
                    || obj == String.class
                    || obj == Integer.class
                    || obj == Long.class
                    || obj == Short.class
                    || obj == Byte.class
                    || obj == Character.class
                    || obj == Boolean.class
                    || obj == Float.class
                    || obj == Double.class);
        } else {
            return (obj instanceof String
                    || obj instanceof Integer
                    || obj instanceof Long
                    || obj instanceof Short
                    || obj instanceof Byte
                    || obj instanceof Character
                    || obj instanceof Boolean
                    || obj instanceof Float
                    || obj instanceof Double);
        }
    }

    /**
     * 将参数对象转换为目标类型
     * @param param 参数对象
     * @param targetType 目标类型
     * @return 目标对象
     */
    public static Object convert(Object param, Class targetType, Map<String, ?> context) {

        // 参数为map就返回context
        if(Map.class.isAssignableFrom(targetType)){
            // map, so return context
            return context;
        }

        // 当前用户
        if(LoginUser.class.isAssignableFrom(targetType)) {
            // current user
            if (LoginContext.get() == null){
                throw new UnAuthorizedException("user isn't login.");
            }
            return LoginContext.get();
        }

        // 目标类型为基本类型
        if (isPrimitive(targetType)) {
            return conversionService.convert(param, targetType);
        }

        // 参数为目标类型子类
        if (param != null && targetType.isAssignableFrom(param.getClass())){
            return param;
        }

        // 若targetType为其他类且param为string, 那么我们将param当做json字符串, 转换为targetType
        if (param != null && param instanceof String){
            try {
                return mapper.readValue(param.toString(), targetType);
            } catch (IOException e) {
                throw new RuntimeException("json 2 obj mapping error: " + param, e);
            }
        }

        return null;
    }

    /**
     * 基本类型转换
     * @param param 参数
     * @param targetType 目标基本类型
     * @return 转换后的参数
     */
    public static Object convertPrimitive(Object param, Class targetType){
        return conversionService.convert(param, targetType);
    }
}
