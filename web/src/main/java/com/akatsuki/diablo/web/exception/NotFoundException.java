package com.akatsuki.diablo.web.exception;

/**
 * Not Found
 */
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = -5775653511116823817L;

    public NotFoundException() {

    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(String message) {
        super(message);
    }
}