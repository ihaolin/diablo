package com.akatsuki.diablo.web.context;

import com.akatsuki.diablo.common.model.LoginUser;

/**
 * 当前用户上下文
 */
public class LoginContext {

	/**
	 * can't instant
	 */
	private LoginContext(){}
	
	private static ThreadLocal<LoginUser> currentUser = new ThreadLocal<LoginUser>();
	
	/**
	 * 设置当前线程用户
	 * @param u 当前用户
	 */
	public static void set(LoginUser u){
		currentUser.set(u);
	}
	
	/**
	 * 获取当前登录用户
	 */
	public static LoginUser get(){
		return currentUser.get();
	}
	
	/**
	 * 获取当前登录用户id
	 */
	public static Long getId(){
		return currentUser.get() == null ? null : currentUser.get().getId();
	}
	
	/**
	 * 获取当前登录用户名
	 */
	public static String getUsername(){
		return currentUser.get() == null ? null : currentUser.get().getNickname();
	}
	
	/**
	 * 清除当前线程用户
	 */
	public static void clear(){
		currentUser.remove();
	}
}
