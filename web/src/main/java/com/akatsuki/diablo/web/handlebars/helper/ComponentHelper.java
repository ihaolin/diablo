package com.akatsuki.diablo.web.handlebars.helper;

import com.akatsuki.diablo.common.model.Response;
import com.akatsuki.diablo.web.handlebars.HandlebarsViewResolver;
import com.akatsuki.diablo.web.handlebars.RenderConstants;
import com.akatsuki.diablo.common.json.Jsoner;
import com.akatsuki.diablo.web.handlebars.component.invoke.Invoker;
import com.github.jknack.handlebars.*;
import com.google.common.base.Throwables;
import com.google.common.collect.Maps;
import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Set;

/**
 * Handlebars视图中的component标签, 可用于渲染不同的组件视图
 * Author: haolin
 * At: 1/6/15
 */
@Component @Slf4j
public class ComponentHelper {

    private static final String COMPONENT = "component";

    @Autowired
    private HandlebarsViewResolver viewResolver;

    @Autowired
    private Invoker invoker;

    @PostConstruct
    public void init(){
        viewResolver.registerHelper(COMPONENT, new Helper<String>() {
            @Override
            public CharSequence apply(String componentPath, Options options) throws IOException {
                // prepare context
                Map<String, Object> allContext = Maps.newHashMap();
                if (options.context.model() instanceof Map) {
                    //noinspection unchecked
                    allContext.putAll((Map<String, Object>) options.context.model());
                    // remove CDATA which may from parent inject
                    @SuppressWarnings("unchecked")
                    Set<String> cdataKeys = (Set<String>) allContext.remove(RenderConstants.CDATA_KEYS);
                    if (cdataKeys != null) {
                        for (String key : cdataKeys) {
                            allContext.remove(key);
                        }
                    }
                }
                // hash
                if (options.hash != null && !options.hash.isEmpty()) {
                    allContext.putAll(options.hash);
                }
                // json
                if (options.tagType == TagType.SECTION && StringUtils.isNotBlank(options.fn.text())) {
                    @SuppressWarnings("unchecked")
                    Map<String, Object> config = Jsoner.nonEmptyJsoner().fromJson(options.fn.text(), Map.class);
                    if (config != null && !config.isEmpty()) {
                        allContext.putAll(config);
                    }
                }
                try {
                    Response<?> invokeResp = invoker.invoke(componentPath, allContext);
                    if (invokeResp != null){
                        if (!invokeResp.isSuccess()){
                            log.error("failed to invoke component of path[{}], ", invokeResp.getError());
                        } else {
                            allContext.put(RenderConstants.DATA, invokeResp.getResult());
                        }
                    }
                    // components: (used to mark this is a component)
                    Template template = viewResolver.getHandlebars().compile("component:" + componentPath + "/main");
                    return new Handlebars.SafeString(template.apply(allContext));
                } catch (Exception e){
                    log.error("failed to invoke component({}), cause: {}", componentPath,
                            Throwables.getStackTraceAsString(e));
                }
                return "";
            }
        });
    }
}
