package com.akatsuki.diablo.web.dispatch;

import com.akatsuki.diablo.web.exception.NotFoundException;
import com.akatsuki.diablo.web.exception.ViewException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.DispatcherServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WrappedDispatcherServlet extends DispatcherServlet {

    private static final long serialVersionUID = 1048939492181326716L;

    private static final Logger log = LoggerFactory.getLogger(WrappedDispatcherServlet.class);

    @Override
    protected void doService(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            super.doService(request, response);
        } catch (NotFoundException e) {
            response.setStatus(404);
        } catch (ViewException e){
            response.setStatus(e.getStatus());
        }
    }
}