package com.akatsuki.diablo.web.handlebars;

import com.akatsuki.diablo.common.util.Protocols;
import com.github.jknack.handlebars.io.AbstractTemplateLoader;
import com.github.jknack.handlebars.io.TemplateSource;
import com.github.jknack.handlebars.io.URLTemplateSource;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import static org.apache.commons.lang3.Validate.notEmpty;

/**
 * Author: haolin
 * On: 1/22/15
 */
public class MultiTemplateLoader extends AbstractTemplateLoader {

    private ServletContext servletContext;

    private String assetsHome;

    public MultiTemplateLoader(ServletContext servletContext, String assetsHome, String prefix, String suffix) {
        this.servletContext = servletContext;
        setPrefix(prefix);
        setSuffix(suffix);
        this.assetsHome = assetsHome;
    }

    @Override
    public TemplateSource sourceAt(String location) throws IOException {
        notEmpty(location, "The uri is required.");
        URL resource = getResource(location);
        if (resource == null) {
            throw new FileNotFoundException(location);
        }
        return new URLTemplateSource(resource.toString(), resource);
    }

    private URL getResource(String location) throws IOException {
        if (Protocols.analyze(location) == Protocols.SERVLET) {
            String uri = resolve(Protocols.removeProtocol(location, Protocols.SERVLET));
            return servletContext.getResource(uri);
        }
        if (location.startsWith("component:")) {
            // 10 is the length of "component:"
            location = "components/" + normalize(location.substring(10));
        } else {
            location = "views/" + normalize(location);
        }
        String uri = assetsHome + location + getSuffix();
        switch (Protocols.analyze(uri)) {
            case HTTP:
            case HTTPS:
                return new URL(uri);
            case FILE:
                File file = new File(uri);
                return file.exists() ? file.toURI().toURL() : null;
            case SERVLET:
                return servletContext.getResource(Protocols.removeProtocol(uri, Protocols.SERVLET));
            default:
                throw new UnsupportedOperationException("template loader only support [HTTP, FILE, ] protocol");
        }
    }
}
