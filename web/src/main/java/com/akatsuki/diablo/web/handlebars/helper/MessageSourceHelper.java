package com.akatsuki.diablo.web.handlebars.helper;

import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Options;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.io.IOException;
import java.util.Locale;

import static org.apache.commons.lang3.Validate.notNull;

public class MessageSourceHelper implements Helper<String> {

  private MessageSource messageSource;

  public MessageSourceHelper(final MessageSource messageSource) {
    this.messageSource = notNull(messageSource, "A message source is required.");
  }

  @Override
  public CharSequence apply(final String code, final Options options)
      throws IOException {
    Object[] args = options.params;
    String defaultMessage = options.hash("default");
    return messageSource.getMessage(code, args, defaultMessage, currentLocale());
  }

  protected Locale currentLocale() {
    return LocaleContextHolder.getLocale();
  }
}