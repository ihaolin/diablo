package com.akatsuki.diablo.web.exception;

/**
 * Author: haolin
 * At: 1/16/15
 */
public class ComponentExcpetion extends RuntimeException {

    private static final long serialVersionUID = 2792054787103222547L;

    public ComponentExcpetion() {
    }

    public ComponentExcpetion(String message) {
        super(message);
    }

    public ComponentExcpetion(Throwable cause) {
        super(cause);
    }

    public ComponentExcpetion(String message, Throwable cause) {
        super(message, cause);
    }
}
