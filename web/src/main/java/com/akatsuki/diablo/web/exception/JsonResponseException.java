package com.akatsuki.diablo.web.exception;

/**
 * ajax异常响应类
 */
public class JsonResponseException extends RuntimeException {

    private int status = 500;

    /**
     * 错误码
     */
    private String code = "error.unknown";

    public JsonResponseException() {}

    public JsonResponseException(String code) {
        this.code = code;
    }

    public JsonResponseException(int status, String code) {
        this.status = status;
        this.code = code;
    }

    public JsonResponseException(int status, String code, Throwable cause) {
        super(code, cause);
        this.code = code;
        this.status = status;
    }

    public JsonResponseException(String code, Throwable cause) {
        super(code, cause);
        this.code = code;
    }

    public JsonResponseException(int status, Throwable cause) {
        super(cause);
        this.code = cause.getMessage();
        this.status = status;
    }

    public JsonResponseException(Throwable cause) {
        super(cause);
        this.code = cause.getMessage();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}