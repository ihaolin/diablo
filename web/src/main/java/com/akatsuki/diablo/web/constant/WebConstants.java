package com.akatsuki.diablo.web.constant;

/**
 * Author: haolin
 * On: 1/11/15
 */
public final class WebConstants {

    /**
     * session中的用户ID
     */
    public static String SESSION_USER_ID = "session_user_id";

}
