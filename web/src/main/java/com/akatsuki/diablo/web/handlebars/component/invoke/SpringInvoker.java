package com.akatsuki.diablo.web.handlebars.component.invoke;

import com.akatsuki.diablo.common.annotation.Export;
import com.akatsuki.diablo.common.exception.ServiceException;
import com.akatsuki.diablo.common.model.Response;
import com.akatsuki.diablo.common.util.Splitters;
import com.akatsuki.diablo.web.exception.ComponentExcpetion;
import com.akatsuki.diablo.web.handlebars.component.HandlebarsComponent;
import com.akatsuki.diablo.web.handlebars.util.ParamConverter;
import com.google.common.base.Throwables;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * Author: haolin
 * At: 1/15/15
 */
@Component @Slf4j
public class SpringInvoker implements HandlebarsComponentInvoker {

    @Autowired
    private ApplicationContext rootContext;

    private final LoadingCache<String, ExportMethod> methodCache;

    public SpringInvoker(){
        methodCache = CacheBuilder.newBuilder().build(new CacheLoader<String, ExportMethod>() {
            @Override
            public ExportMethod load(String service) throws ServiceException{
                Class<?> beanClass;
                Object bean;
                Method method;
                String[] paramNames;
                Class<?>[] paramTypes;
                try {
                    List<String> parts = Splitters.COLON.splitToList(service);
                    String beanName = parts.get(0);
                    String methodName = parts.get(1);
                    beanClass = Class.forName(beanName);
                    bean = rootContext.getBean(beanClass);
                    method = findMethodByName(beanClass, methodName);
                    Export export = method.getAnnotation(Export.class);
                    if (export == null){
                        log.error("method() isn't exported.", service);
                        throw new ServiceException("method.isnt.export");
                    }
                    paramNames = export.params();
                    paramTypes = method.getParameterTypes();
                    if (paramTypes.length != paramNames.length){
                        log.error("export params length({}) isn't matched method parameters length()",
                                paramNames.length, paramTypes.length);
                        throw new ServiceException("method.params.not.matched");
                    }
                } catch (ClassNotFoundException | NoSuchMethodException e) {
                    log.error("spring service bean not found: {}", service);
                    throw new ServiceException("component.not.found");
                }
                return new ExportMethod(bean, method, paramNames, paramTypes);
            }
        });
    }

    @Override
    public Response<?> invoke(HandlebarsComponent component, Map<String, Object> context) {
        try {
            ExportMethod method = methodCache.get(component.getService());
            Object[] params = extractParams(context, method.paramNames, method.paramTypes);
            return (Response<?>)method.invoke(params);
        } catch (Exception e){
            log.error("invoke component({}) fail.", component);
            throw new ServiceException("component.invoke.fail");
        }
    }

    private Object[] extractParams(Map<String, Object> context, String[] paramNames, Class<?>[] paramTypes) {
        if (paramNames == null || paramNames.length <= 0){
            return null;
        }
        Object[] params = new Object[paramNames.length];
        String paramName;
        Class<?> paramType;
        for (int i=0; i<paramNames.length; i++){
            paramName = paramNames[i];
            paramType = paramTypes[i];
            params[i] = ParamConverter.convert(context.get(paramName), paramType, context);
        }
        return params;
    }

    private Method findMethodByName(Class<?> beanClazz, String methodName) throws NoSuchMethodException {
        Method[] methods = beanClazz.getMethods();
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        throw new NoSuchMethodException();
    }

    @AllArgsConstructor @ToString
    private static class ExportMethod{

        /**
         * spring bean
         */
        Object bean;

        /**
         * bean method
         */
        Method method;

        /**
         * param names
         */
        String[] paramNames;

        /**
         * param types
         */
        Class<?>[] paramTypes;

        public Object invoke(Object[] args) throws InvocationTargetException, IllegalAccessException {
            return method.invoke(bean, args);
        }
    }
}
