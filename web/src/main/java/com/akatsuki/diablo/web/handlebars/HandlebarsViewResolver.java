package com.akatsuki.diablo.web.handlebars;

import com.akatsuki.diablo.web.exception.NotFoundException;
import com.akatsuki.diablo.web.handlebars.helper.MessageSourceHelper;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.HelperRegistry;
import com.github.jknack.handlebars.ValueResolver;
import com.github.jknack.handlebars.helper.DefaultHelperRegistry;
import com.github.jknack.handlebars.helper.I18nHelper;
import com.github.jknack.handlebars.helper.I18nSource;
import com.github.jknack.handlebars.io.AbstractTemplateLoader;
import com.github.jknack.handlebars.io.TemplateLoader;
import com.github.jknack.handlebars.io.URLTemplateLoader;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.view.AbstractTemplateViewResolver;
import org.springframework.web.servlet.view.AbstractUrlBasedView;

import javax.servlet.ServletContext;
import java.io.*;
import java.net.URI;
import java.util.*;
import java.util.Map.Entry;
import static org.apache.commons.lang3.Validate.notEmpty;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * A Handlebars {@link org.springframework.web.servlet.ViewResolver view resolver}.
 *
 * @author edgar.espina
 * @since 0.1
 */
public class HandlebarsViewResolver extends AbstractTemplateViewResolver
        implements InitializingBean, HelperRegistry {

    /**
     * 默认内容类型
     */
    public static final String DEFAULT_CONTENT_TYPE = "text/html;charset=UTF-8";

    /**
     * Handlebars核心对象，用于渲染hbs
     */
    private Handlebars handlebars;

    /**
     * 视图解析器, 默认支持JavaBean和Map
     */
    private ValueResolver[] valueResolvers = ValueResolver.VALUE_RESOLVERS;

    /**
     * Fail on missing file. Default is: true.
     */
    private boolean failOnMissingFile = true;

    /**
     * 扩展解析注册中心, 可通过registry.registerHelper来扩展自己的解析标签
     */
    private HelperRegistry registry = new DefaultHelperRegistry();

    /** True, if the message helper (based on {@link org.springframework.context.MessageSource}) should be registered. */
    private boolean registerMessageHelper = true;

    /**
     * If true, the i18n helpers will use a {@link org.springframework.context.MessageSource} instead of a plain
     * {@link java.util.ResourceBundle} .
     */
    private boolean bindI18nToMessageSource;

    /**
     * 标志block标签被partial标签引用后，是否删除
     * <pre>
     * {{#block "footer" delete-after-merge=true}}
     * </pre>
     *
     */
    private boolean deletePartialAfterMerge;

    /**
     * 前端资源文件
     */
    private String assetsHome;

    public String getAssetsHome() {
        return assetsHome;
    }

    public void setAssetsHome(String assetsHome) {
        this.assetsHome = assetsHome;
    }

    /**
     * default equals ${prefix}/component;
     */
    private String componentPath;

    public String getComponentPath() {
        return componentPath;
    }

    public void setComponentPath(String componentPath) {
        this.componentPath = componentPath;
    }

    @Autowired
    private ServletContext servletContext;

    /**
     * Creates a new {@link HandlebarsViewResolver}.
     *
     * @param viewClass The view's class. Required.
     */
    public HandlebarsViewResolver(
            final Class<? extends HandlebarsView> viewClass) {
        setViewClass(viewClass);
        setContentType(DEFAULT_CONTENT_TYPE);
        setPrefix(TemplateLoader.DEFAULT_PREFIX);
        setSuffix(TemplateLoader.DEFAULT_SUFFIX);
    }

    /**
     * Creates a new {@link HandlebarsViewResolver}.
     */
    public HandlebarsViewResolver() {
        this(HandlebarsView.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected AbstractUrlBasedView buildView(final String viewName)
            throws Exception {
        return configure((HandlebarsView) super.buildView(viewName));
    }

    @Override
    public HandlebarsView resolveViewName(final String viewName, final Locale locale)
            throws Exception {
        return (HandlebarsView) super.resolveViewName(viewName, locale);
    }

    protected AbstractUrlBasedView configure(final HandlebarsView view)
            throws IOException {
        String url = view.getUrl();
        // Remove prefix & suffix.
        url = url.substring(getPrefix().length(), url.length()
                - getSuffix().length());
        // Compile the template.
        try {
            view.setTemplate(handlebars.compile(url));
            view.setValueResolver(valueResolvers);
        } catch (FileNotFoundException e){
            // 404
            throw new NotFoundException();
        } catch (IOException ex) {
            if (failOnMissingFile) {
                throw ex;
            }
            logger.debug("File not found: " + url);
        }
        return view;
    }

    @Override
    protected Class<?> requiredViewClass() {
        return HandlebarsView.class;
    }

    @Override
    public void afterPropertiesSet() {

        //setPrefix(assetsHome + "/views");
        //setComponentPath(assetsHome + "/components");

        // Creates a new template loader.
        // URLTemplateLoader templateLoader = createTemplateLoader(getApplicationContext());

        // Creates a new handlebars object.
        handlebars = notNull(createHandlebars(new MultiTemplateLoader(servletContext, assetsHome, getPrefix(), getSuffix())),
                "A handlebars object is required.");

        handlebars.with(registry);

        if (registerMessageHelper) {
            // Add a message source helper
            handlebars.registerHelper("message", new MessageSourceHelper(getApplicationContext()));
        }

        if (bindI18nToMessageSource) {
            I18nSource i18nSource = createI18nSource(getApplicationContext());

            I18nHelper.i18n.setSource(i18nSource);
            I18nHelper.i18nJs.setSource(i18nSource);
        }

        // set delete partial after merge
        handlebars.setDeletePartialAfterMerge(deletePartialAfterMerge);
    }

    private static I18nSource createI18nSource(final ApplicationContext context) {
        return new I18nSource() {
            @Override
            public String message(final String key, final Locale locale, final Object... args) {
                return context.getMessage(key, args, locale);
            }

            @Override
            public String[] keys(final String basename, final Locale locale) {
                ResourceBundle bundle = ResourceBundle.getBundle(basename, locale);
                Enumeration<String> keys = bundle.getKeys();
                List<String> result = new ArrayList<String>();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    result.add(key);
                }
                return result.toArray(new String[result.size()]);
            }
        };
    }

    protected Handlebars createHandlebars(final AbstractTemplateLoader templateLoader) {
        return new Handlebars(templateLoader);
    }

    protected URLTemplateLoader createTemplateLoader(
            final ApplicationContext context) {
        URLTemplateLoader templateLoader = new SpringTemplateLoader(context);
        // Override prefix and suffix.
        templateLoader.setPrefix(getPrefix());
        templateLoader.setSuffix(getSuffix());
        return templateLoader;
    }

    public Handlebars getHandlebars() {
        if (handlebars == null) {
            throw new IllegalStateException(
                    "afterPropertiesSet() method hasn't been call it.");
        }
        return handlebars;
    }

    public void setValueResolvers(final ValueResolver... valueResolvers) {
        this.valueResolvers = notEmpty(valueResolvers,
                "At least one value-resolver must be present.");
    }

    public void setFailOnMissingFile(final boolean failOnMissingFile) {
        this.failOnMissingFile = failOnMissingFile;
    }

    public void setHelpers(final Map<String, Helper<?>> helpers) {
        notNull(helpers, "The helpers are required.");
        for (Entry<String, Helper<?>> helper : helpers.entrySet()) {
            registry.registerHelper(helper.getKey(), helper.getValue());
        }
    }

    public void setHelpers(final List<Object> helpers) {
        notNull(helpers, "The helpers are required.");
        for (Object helper : helpers) {
            registry.registerHelpers(helper);
        }
    }

    @Override
    public HandlebarsViewResolver registerHelpers(final Object helperSource) {
        registry.registerHelpers(helperSource);
        return this;
    }

    @Override
    public HandlebarsViewResolver registerHelpers(final Class<?> helperSource) {
        registry.registerHelpers(helperSource);
        return this;
    }

    @Override
    public <C> Helper<C> helper(final String name) {
        return registry.helper(name);
    }

    @Override
    public Set<Entry<String, Helper<?>>> helpers() {
        return registry.helpers();
    }

    @Override
    public <H> HandlebarsViewResolver registerHelper(final String name, final Helper<H> helper) {
        registry.registerHelper(name, helper);
        return this;
    }

    @Override
    public <H> HandlebarsViewResolver registerHelperMissing(final Helper<H> helper) {
        registry.registerHelperMissing(helper);
        return this;
    }

    @Override
    public HandlebarsViewResolver registerHelpers(final URI location) throws Exception {
        registry.registerHelpers(location);
        return this;
    }

    @Override
    public HandlebarsViewResolver registerHelpers(final File input) throws Exception {
        registry.registerHelpers(input);
        return this;
    }

    @Override
    public HandlebarsViewResolver registerHelpers(final String filename, final Reader source)
            throws Exception {
        registry.registerHelpers(filename, source);
        return this;
    }

    @Override
    public HandlebarsViewResolver registerHelpers(final String filename, final InputStream source)
            throws Exception {
        registry.registerHelpers(filename, source);
        return this;
    }

    @Override
    public HandlebarsViewResolver registerHelpers(final String filename, final String source)
            throws Exception {
        registry.registerHelpers(filename, source);
        return this;
    }

    public HandlebarsViewResolver withoutMessageHelper() {
        setRegisterMessageHelper(false);
        return this;
    }

    public void setRegisterMessageHelper(final boolean registerMessageHelper) {
        this.registerMessageHelper = registerMessageHelper;
    }

    public void setBindI18nToMessageSource(final boolean bindI18nToMessageSource) {
        this.bindI18nToMessageSource = bindI18nToMessageSource;
    }

    public void setDeletePartialAfterMerge(final boolean deletePartialAfterMerge) {
        this.deletePartialAfterMerge = deletePartialAfterMerge;
    }
}