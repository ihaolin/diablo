package com.akatsuki.diablo.web.handlebars.view;

import com.akatsuki.diablo.web.context.LoginContext;
import com.akatsuki.diablo.web.handlebars.RenderConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;

/**
 * Author: haolin
 * At: 1/10/15
 */
@Controller
public class ViewEntrance {

    @RequestMapping
    public String entry(HttpServletRequest request, Model model){
        String viewPath = request.getRequestURI();
        initContext(request, model);
        return viewPath;
    }

    private void initContext(HttpServletRequest req, Model model) {
        model.addAttribute(RenderConstants.USER, LoginContext.get());
        if (req != null) {
            for (Object name : req.getParameterMap().keySet()) {
                model.addAttribute((String) name, req.getParameter((String) name));
            }
        }
    }
}
