package com.akatsuki.diablo.web.handlebars.component;

import lombok.Data;
import java.io.Serializable;

/**
 * 渲染组件
 * Author: haolin
 * At: 1/15/15
 */
@Data
public class HandlebarsComponent implements Serializable{

    private static final long serialVersionUID = 4421316110341009913L;

    /**
     * 路径
     */
    private String path;

    /**
     * 服务接口名(全类名:方法名)
     */
    private String service;

    /**
     * 组件类型
     * {@link Type}
     */
    private String type;

    public enum Type{

        SPRING("SPRING"),

        HTTP("HTTP");

        private String value;

        private Type(String value){
            this.value = value;
        }

        public String value(){
            return this.value;
        }
    }

}
