package com.akatsuki.diablo.web.converter;

import com.akatsuki.diablo.common.json.Jsoner;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.util.Assert;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Author: haolin
 * At: 1/13/15
 */
public class JsonMessageWrappedConverter extends AbstractHttpMessageConverter<Object>{

        public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

        private ObjectMapper objectMapper = Jsoner.nonEmptyJsoner().getMapper();

        private boolean prefixJson = false;

        /**
         * Construct a new {@code BindingJacksonHttpMessageConverter}.
         */
        public JsonMessageWrappedConverter() {
            super(new MediaType("application", "json", DEFAULT_CHARSET), new MediaType("application", "javascript", DEFAULT_CHARSET));
        }

        /**
         * Set the {@code ObjectMapper} for this view. If not set, a default
         * {@link ObjectMapper#ObjectMapper() ObjectMapper} is used.
         * <p>Setting a custom-configured {@code ObjectMapper} is one way to take further control of the JSON
         * serialization process. For example, an extended
         * can be configured that provides custom serializers for specific types. The other option for refining
         * the serialization process is to use Jackson's provided annotations on the types to be serialized,
         * in which case a custom-configured ObjectMapper is unnecessary.
         */
        public void setObjectMapper(ObjectMapper objectMapper) {
            Assert.notNull(objectMapper, "ObjectMapper must not be null");
            this.objectMapper = objectMapper;
        }

        /**
         * Return the underlying {@code ObjectMapper} for this view.
         */
        public ObjectMapper getObjectMapper() {
            return this.objectMapper;
        }

        /**
         * Indicate whether the JSON output by this view should be prefixed with "{} &&". Default is false.
         * <p>Prefixing the JSON string in this manner is used to help prevent JSON Hijacking.
         * The prefix renders the string syntactically invalid as a script so that it cannot be hijacked.
         * This prefix does not affect the evaluation of JSON, but if JSON validation is performed on the
         * string, the prefix would need to be ignored.
         */
        public void setPrefixJson(boolean prefixJson) {
            this.prefixJson = prefixJson;
        }


        @Override
        public boolean canRead(Class<?> clazz, MediaType mediaType) {
            JavaType javaType = getJavaType(clazz);
            return (this.objectMapper.canDeserialize(javaType) && canRead(mediaType));
        }

        @Override
        public boolean canWrite(Class<?> clazz, MediaType mediaType) {
            return (this.objectMapper.canSerialize(clazz) && canWrite(mediaType));
        }

        @Override
        protected boolean supports(Class<?> clazz) {
            // should not be called, since we override canRead/Write instead
            throw new UnsupportedOperationException();
        }

        @Override
        protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage)
                throws IOException, HttpMessageNotReadableException {

            JavaType javaType = getJavaType(clazz);
            try {
                return this.objectMapper.readValue(inputMessage.getBody(), javaType);
            } catch (IOException ex) {
                throw new HttpMessageNotReadableException("Could not read JSON: " + ex.getMessage(), ex);
            }
        }

        @Override
        protected void writeInternal(Object object, HttpOutputMessage outputMessage)
                throws IOException, HttpMessageNotWritableException {

            JsonEncoding encoding = getJsonEncoding(outputMessage.getHeaders().getContentType());
            JsonGenerator jsonGenerator =
                    this.objectMapper.getFactory().createGenerator(outputMessage.getBody(), encoding);
            try {
                if (this.prefixJson) {
                    jsonGenerator.writeRaw("{} && ");
                }
                Object result;
                if(object instanceof JSONPObject){
                    JSONPObject jsonP = ((JSONPObject) object);
                    result = new JSONPObject(jsonP.getFunction(), ImmutableMap.of("status", 200, "result", jsonP.getValue()));
                } else {
                    result = ImmutableMap.of("status", 200, "result", object);
                }
                this.objectMapper.writeValue(jsonGenerator, result);
            } catch (IOException ex) {
                throw new HttpMessageNotWritableException("Could not write JSON: " + ex.getMessage(), ex);
            }
        }

        protected JavaType getJavaType(Class<?> clazz) {
            return objectMapper.constructType(clazz);
        }

        protected JsonEncoding getJsonEncoding(MediaType contentType) {
            if (contentType != null && contentType.getCharSet() != null) {
                Charset charset = contentType.getCharSet();
                for (JsonEncoding encoding : JsonEncoding.values()) {
                    if (charset.name().equals(encoding.getJavaName())) {
                        return encoding;
                    }
                }
            }
            return JsonEncoding.UTF8;
        }
}
