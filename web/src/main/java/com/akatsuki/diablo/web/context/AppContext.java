package com.akatsuki.diablo.web.context;

import com.akatsuki.diablo.common.util.Splitters;
import com.google.common.base.MoreObjects;
import java.util.List;

/**
 * Author: haolin
 * At: 1/13/15
 */
public class AppContext {

    private static ThreadLocal<String> host = new ThreadLocal<String>();
    private static ThreadLocal<String> locale = new ThreadLocal<String>();

    public static String getHost(){
        return host.get();
    }

    public static void setHost(String h){
        host.set(h);
    }

    public static String getDomain(){
        return Splitters.COLON.splitToList(getHost()).get(0);
    }

    public static Integer getPort(){
        List<String> parts = Splitters.COLON.splitToList(getHost());
        if (parts.size() <= 1){
            return 80;
        }
        return MoreObjects.firstNonNull(Integer.valueOf(parts.get(1)), 80);
    }

    public static void setLocale(String l) {
        locale.set(l);
    }

    public static String getLocale(){
        return locale.get();
    }

    public static void clear() {
        host.remove();
        locale.remove();
    }
}
