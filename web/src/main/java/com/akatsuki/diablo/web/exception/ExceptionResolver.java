package com.akatsuki.diablo.web.exception;

import com.akatsuki.diablo.common.exception.ServiceException;
import com.akatsuki.diablo.common.json.Jsoner;
import com.akatsuki.diablo.web.message.MessageSources;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Throwables;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.lang.reflect.Method;

/**
 * 异常处理器
 */
@Slf4j
public class ExceptionResolver extends ExceptionHandlerExceptionResolver {

    private String defaultErrorView;

    private static final String ERROR_UNKNOWN = "error.unknown";

    private static final String ERROR_ARGS = "args.invalid";

    private Jsoner jsoner = Jsoner.nonEmptyJsoner();

    public String getDefaultErrorView() {
        return defaultErrorView;
    }

    public void setDefaultErrorView(String defaultErrorView) {
        this.defaultErrorView = defaultErrorView;
    }

    @Autowired
    private MessageSources messageSources;

    @Override
    protected ModelAndView doResolveHandlerMethodException(HttpServletRequest request,
                                                           HttpServletResponse response,
                                                           HandlerMethod handlerMethod, Exception exception) {
        if (handlerMethod == null) {
            return null;
        }

        Method method = handlerMethod.getMethod();
        if (method == null) {
            return null;
        }

        Throwables.propagateIfInstanceOf(exception, NotFoundException.class);
        Error error = buildError(exception);
        // ajax
        ResponseBody responseBodyAnn = AnnotationUtils.findAnnotation(method, ResponseBody.class);
        if (Objects.equal(request.getHeader(HttpHeaders.X_REQUESTED_WITH), "XMLHttpRequest")
                || responseBodyAnn != null) {
            PrintWriter out = null;
            try {
                response.setContentType(MediaType.JSON_UTF_8.toString());
                response.setStatus(error.getStatus());
                out = response.getWriter();
                out.print(jsoner.toJson(error));
                return null;
            } catch (Exception e) {
                return null;
            } finally {
                if (out != null) {
                    out.close();
                }
            }
        }
        throw new ViewException(error.getStatus());
    }

    /**
     * 获取异常的错误信息
     * @param exception 异常对象
     * @return messageSources.get后的内容
     */
    private Error buildError(Exception exception) {
        if (exception instanceof JsonResponseException) {
            JsonResponseException jsonEx = (JsonResponseException) exception;
            Integer status = MoreObjects.firstNonNull(jsonEx.getStatus(), 500);
            return new Error(status, MoreObjects.firstNonNull(messageSources.get(jsonEx.getCode()), messageSources.get(ERROR_UNKNOWN)));
        } else if (exception instanceof ServiceException) {
            ServiceException serviceEx = (ServiceException) exception;
            return new Error(500, MoreObjects.firstNonNull(messageSources.get(serviceEx.getMessage()), messageSources.get(ERROR_UNKNOWN)));
        } else if (exception instanceof BindException) {
            BindException bindException = (BindException) exception;
            BindingResult result = bindException.getBindingResult();
            return new Error(400, MoreObjects.firstNonNull(messageSources.get(result.getFieldError().getDefaultMessage()), messageSources.get(ERROR_UNKNOWN)));
        } else if(exception instanceof IllegalArgumentException){
            return new Error(400, MoreObjects.firstNonNull(messageSources.get(exception.getMessage()), messageSources.get(ERROR_ARGS)));
        }
        return new Error(500, messageSources.get(ERROR_UNKNOWN));
    }

    @Data
    @AllArgsConstructor
    private static class Error{

        /**
         * 状态码
         */
        private Integer status;

        /**
         * 错误信息
         */
        private String msg;
    }
}