package com.akatsuki.diablo.web.exception;

import lombok.Data;

/**
 * Author: haolin
 * At: 1/15/15
 */
@Data
public class ViewException extends RuntimeException{

    private static final long serialVersionUID = -5442717210260160057L;

    private int status;

    public ViewException() {
    }

    public ViewException(int status){
        this.status = status;
    }

    public ViewException(Throwable cause) {
        super(cause);
    }

    public ViewException(String message, Throwable cause) {
        super(message, cause);
    }

    public ViewException(String message) {
        super(message);
    }
}
