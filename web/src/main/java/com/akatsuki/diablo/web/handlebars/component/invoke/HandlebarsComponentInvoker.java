package com.akatsuki.diablo.web.handlebars.component.invoke;

import com.akatsuki.diablo.common.model.Response;
import com.akatsuki.diablo.web.handlebars.component.HandlebarsComponent;
import java.util.Map;

/**
 * Author: haolin
 * At: 1/16/15
 */
public interface HandlebarsComponentInvoker {

    /**
     * 组件调用
     * @param component 组件
     * @param context 上下文
     * @return 结果
     */
    Response<?> invoke(HandlebarsComponent component, Map<String, Object> context);
}
