package com.akatsuki.diablo.web.handlebars.component;

import lombok.Data;
import java.io.Serializable;

/**
 * 渲染路径, 如/cos_pts/1
 * Author: haolin
 * At: 1/15/15
 */
@Data
public class HandlebarsMapping implements Serializable{

    private static final long serialVersionUID = -2507552142862765195L;

    /**
     * 路径
     */
    private String path;

    /**
     * 视图
     */
    private String view;

}
