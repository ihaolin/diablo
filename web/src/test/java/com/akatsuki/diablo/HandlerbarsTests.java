package com.akatsuki.diablo;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.common.io.Resources;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

/**
 * Author: haolin
 * On: 1/22/15
 */
public class HandlerbarsTests {

    private Handlebars handlebars = new Handlebars();

    @Test
    public void testCompile() throws IOException, URISyntaxException {
        String path = "/Users/haolin/Working/projects/milo/public/views/login.hbs";
        Template template = handlebars.compileInline(Files.toString(new File(path), Charset.forName("UTF-8")));
        System.out.println(template.apply(new Object()));
    }

}
